set timef=%time%
set timef=%timef::=.%
::Retire les millisecondes
set timef=%timef:~0,-3%

robocopy "." "..\Backups\%date% - %timef%" /E

pause